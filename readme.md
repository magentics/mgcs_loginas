Mgcs_LoginAs
============

Log in on the frontend as a customer by clicking a link in the backend.

Facts
-----

* Version: See tags list
* Module name: Mgcs_LoginAs
* Magento Connect 1.0 extension key: n/a
* Magento Connect 2.0 extension key: n/a

Description
-----------

This module enables you to login to the frontend with one click from the backend. The module shows a link for every store on the customer view page in the backend. With that link, you can log in to the frontend as that customer.

![Log In As Customer](https://bitbucket.org/magentics/mgcs_loginas/raw/master/loginas.png)

Compatibility
-------------

* Magento CE 1.6+
* Magento EE 1.11+

Installation
------------

* Use [Modman](https://github.com/colinmollenhour/modman)
* Enter a secret key in the backend under Systen | Configuration | Customers | Customer Configuration | Login As Customer - it can be anything, it's used to make the hash that is used to log in a customer more secure. To make it even more secure, you can enter a different hash for each store view.
* See the module in action: Customers | Manage Customers and click a random customer. On the 'Customer View' tab, you will find the link(s) to log in as that customer.

Uninstallation
--------------

1. Remove all extension files from your Magento installation

Support
-------

If you have any issues with this extension, open an issue on [Bitbucket](https://bitbucket.org/magentics/mgcs_loginas)

Contribution
------------

Any contribution is highly appreciated. The best way to contribute code is to open a [pull request on Bitbucket](https://www.atlassian.com/git/tutorials/making-a-pull-request).

Lead Developers
---------------

Mark van der Sanden http://www.magentics.nl/ [@magentics_nl](https://twitter.com/magentics_nl)

License
-------

[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)