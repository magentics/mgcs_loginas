<?php

class Mgcs_LoginAs_Block_Adminhtml_Customer_Edit_Tab_View_Loginas extends Mage_Adminhtml_Block_Abstract
{
    protected $_customer;

    protected function _toHtml()
    {
        if (!Mage::getSingleton('admin/session')->isAllowed('admin/customer/mgcs_loginas')) {
            return '';
        }
        return parent::_toHtml();
    }

    /**
     * Is a secret configured on the global level?
     *
     * @return bool
     */
    public function isSecretConfigured()
    {
        $secret = Mage::helper('mgcs_loginas')->getSecret(0);
        return $secret > '';
    }

    /**
     * Get the login url for a specific store
     *
     * Returns
     *
     * @param mixed $store
     */
    public function getLoginUrl($store)
    {
        return Mage::helper('mgcs_loginas')->getLoginUrl($this->getCustomer(), $store);
    }

    public function getCustomer()
    {
        if (!$this->_customer) {
            $this->_customer = Mage::registry('current_customer');
        }
        return $this->_customer;
    }

    /**
     * Get all stores a user can log in to
     *
     * @return Mage_Core_Model_Store[]
     */
    public function getFrontendStores()
    {
        $stores = array();
        $websiteIds = $this->getCustomer()->getSharedWebsiteIds();
        foreach ($websiteIds as $websiteId) {
            foreach (Mage::app()->getWebsite($websiteId)->getStores() as $store) {
                if ($store->getIsActive()) {
                    $stores[] = $store;
                }
            }
        }
        return $stores;
    }

}