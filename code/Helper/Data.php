<?php

class Mgcs_LoginAs_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_SECRET = 'customer/mgcs_loginas/secret';
    const XML_PATH_EMPTY_CART = 'customer/mgcs_loginas/empty_cart';


    /**
     * Get the frontend URL for logging in a customer
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param mixed $store  Store id or object
     * @return string
     */
    public function getLoginUrl(Mage_Customer_Model_Customer $customer, $store = null)
    {
        $hash = $this->calculateHash($customer, $store);
        if (!$hash) {
            return null;
        }
        $url = Mage::getUrl('loginas/customer/login', array(
            '_store'   => $store,
            'customer' => $customer->getId(),
            'hash'     => $hash,
        ));

        return $url;
    }


    /**
     * Calculcate the hash used for loggin in a customer
     *
     * It takes some customer characteristics and hashes it together with the
     * time (hashes are valid an hour max) and a secret key that is in the
     * settings.
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param mixed $store  Store id or object
     * @return string|null  The calculated hash - returns NULL if there is no secret key set
     */
    public function calculateHash(Mage_Customer_Model_Customer $customer, $store = null)
    {
        if ($store == null) {
            $store = Mage::app()->getDefaultStoreView();
        }
        $store = Mage::app()->getStore($store);

        $secret = $this->getSecret($store);
        if (!$secret) {
            return null;
        }

        $data = array(
            $store->getId(),
            $customer->getEmail(),
            $customer->getId(),
            date('Y-m-d-H'), // only valid one hour max
            $secret,
        );
        $hash = sha1(implode(',', $data));

        return $hash;
    }

    /**
     * Get a random, readable (a-zA-Z0-9), string
     *
     * It uses `openssl_random_pseudo_bytes()` if available, otherwise it uses
     * `mt_rand()` to compose a random string
     *
     * @param int $length
     * @return string
     */
    public function getRandomString($length = 32)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            return substr(base64_encode(openssl_random_pseudo_bytes($length)), 0, $length);
        }
        $result = '';
        $chars  = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        while (strlen($result) < $length) {
            $result .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $result;
    }

    /**
     * Config: Empty guest cart before login?
     *
     * @param mixed $store  Store id or store model (scope of the configuration) - defaults to current store
     * @return void
     */
    public function shouldEmptyCart($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_EMPTY_CART, $store);
    }

    /**
     * Get the configured secret
     *
     * @param mixed $store  Store id or store model (scope of the configuration) - defaults to current store
     * @return string
     */
    public function getSecret($store = null)
    {
         return Mage::getStoreConfig(self::XML_PATH_SECRET, $store);
    }

}