<?php

if (!Mage::getStoreConfig(Mgcs_LoginAs_Helper_Data::XML_PATH_SECRET, 0)) {
    $config = Mage::getConfig();
    $config->saveConfig(Mgcs_LoginAs_Helper_Data::XML_PATH_SECRET, Mage::helper('mgcs_loginas')->getRandomString(), 'default', 0);
}
