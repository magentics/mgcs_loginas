<?php

class Mgcs_LoginAs_CustomerController extends Mage_Core_Controller_Front_Action
{

    public function loginAction()
    {
        $customerId = $this->getRequest()->getParam('customer');
        $hash       = $this->getRequest()->getParam('hash');

        $customer = Mage::getModel('customer/customer')->load($customerId);
        if (!$customer->getId()) {
            return $this->_redirect('customer/account/login');
        }
        $calculatedHash = Mage::helper('mgcs_loginas')->calculateHash($customer, Mage::app()->getStore());
        $session = Mage::getSingleton('customer/session');

        try {
            if ($calculatedHash == $hash) {
                if ($session->isLoggedIn()) {
                    // first log out the customer and unset the session cookie
                    // pass the correct parameters, because they get passed back by the logout action
                    return $this->_redirect('*/*/logout', array(
                        'customer' => $customerId,
                        'hash'     => $hash,
                    ));
                }
                if (Mage::helper('mgcs_loginas')->shouldEmptyCart()) {
                    // empty the cart before login (to prevent merging guest cart and saved cart)
                    $cartQty = Mage::helper('checkout/cart')->getSummaryCount();
                    if ($cartQty > 0) {
                        $session->addWarning($this->__('Your guest cart contained %d items, which have been removed before logging in.', $cartQty));
                    }
                    Mage::getSingleton('checkout/session')->clear();
                }
                $session->setCustomerAsLoggedIn($customer);
                Mage::log(sprintf('[mgcs_loginas] Logged in customer #%d', $customer->getId()));
                $session->addSuccess($this->__('Successfully logged in as customer'));
                return $this->_redirect('customer/account');
            } else {
                Mage::throwException($this->__('There was an error logging you in: hash mismatch.'));
            }
        } catch (Mage_Core_Exception $e) {
            $session->addError($e->getMessage());
        } catch (Exception $e) {
            $session->addError($this->__('There was an error logging you in. The details have been logged.'));
            Mage::logException($e);
        }
        return $this->_redirect('customer/account/login');
    }

    public function logoutAction()
    {
        $customerId = $this->getRequest()->getParam('customer');
        $hash       = $this->getRequest()->getParam('hash');

        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {
            Mage::log(sprintf('[mgcs_loginas] Logged out customer #%d', $session->getCustomer()->getId()));
        }
        $session->logout();

        // now log in the customer with the same parameters
        return $this->_redirect('*/*/login', array(
            'customer' => $customerId,
            'hash'     => $hash,
        ));
    }

}